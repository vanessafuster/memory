﻿/*En esta clase se establecen los atributos propios de los administradores, con su método constructortos. 
 *  También contiene el método que desarrolla el menú de los usuarios tipo Administrador*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuegoMemory
{
    class Administrador : Usuario
    {
        static string simbolo;
        //METODO CONSTRUCTOR
        public Administrador(string nick, string contrasenya) : base( nick, contrasenya)
        {
            nick = "admin";
            contrasenya = "01234";
        }
        //SE SOBREESCRIBE EL MÉTODO ToString() PARA AÑADIR LA INFORMACIÓN A EL ARCHIVO
        public override string ToString()
        {
            return base.ToString();
        }
        //MÉTODO QUE MUESTRA LA INFORMACION DE LOS USUARIOS POR PANTALLA
        public override string Mostrar()
        {
            return "Nick: " + Nick + "Contraseña: " + Contrasenya;
        }
        //MÉTODO QUE CONTIENE EL MENÚ DEL USUARIO TIPO ADMINISTRADOR
        public static void MenuAdministrador()
        {
            string opcionMenu;
            int indiceEliminar;
            do
            {
                Console.WriteLine();
                Console.WriteLine("======MENÚ ADMINISTRADOR======");
                Console.WriteLine();
                Console.WriteLine("Elija la opción que desea:");
                Console.WriteLine("1. Listar usuarios");
                Console.WriteLine("2. Listar símbolos");
                Console.WriteLine("3. Eliminar símbolo");
                Console.WriteLine("4. Añadir símbolo");
                Console.WriteLine("5. Resetear ranking");
                Console.WriteLine("0. Salir del programa");
                Console.WriteLine();
                opcionMenu = Console.ReadLine();
                Console.WriteLine();

                switch (opcionMenu)
                {
                    case "1"://LISTAR USUARIOS
                        {
                            foreach (KeyValuePair<string, Usuario> kvp in usuariosList)
                            {
                                Console.WriteLine("{0}", kvp.Value.Mostrar());
                            }
                        }
                        break;
                    case "2"://LISTAR SIMBOLOS
                        {
                            Console.WriteLine();
                            Console.WriteLine("Listado de símbolos disponibles");
                            Console.WriteLine();
                           for(int i=0; i<Program.simbolosOrdenados.Count;i++)
                            {
                                Console.WriteLine(i + ". " + Program.simbolosOrdenados[i]);
                            }
                        }
                        break;
                    case "3"://ELIMINAR SIMBOLO
                        {
                            Console.WriteLine("ADVERTENCIA:");
                            Console.WriteLine("Si elimina algún símbolo y no añade ningún otro en su lugar," +
                                " el programa podría fallar");
                            Console.WriteLine();
                            for (int i = 0; i < Program.simbolosOrdenados.Count; i++)
                            {
                                Console.WriteLine(i + ". " + Program.simbolosOrdenados[i]);
                            }
                            do
                            {
                                Console.WriteLine("Indique el índice del símnolo que quiera eliminar");
                                Console.WriteLine();
                            } while ((!Int32.TryParse(Console.ReadLine(), out indiceEliminar))||
                                (indiceEliminar<0|| indiceEliminar>Program.simbolosOrdenados.Count));
                            
                            Program.simbolosOrdenados.RemoveAt(indiceEliminar);
                            Console.WriteLine("Símbolo eliminado");
                        }
                        break;
                    case "4"://AÑADIR SIMBOLO
                        {
                            do {
                                Console.WriteLine("Introduzca el símbolo a añadir al listado:");
                                simbolo = Console.ReadLine();
                                if (String.IsNullOrEmpty(simbolo))
                                {
                                    Console.WriteLine("El símbolo no puede estar vacío");
                                }
                            } while (String.IsNullOrEmpty(simbolo));
                            Program.simbolosOrdenados.Add(simbolo);
                            Console.WriteLine("Símbolo añadido");
                        }
                        break;

                    case "0": //salir;
                        {
                            Program.GrabarSimbolos();
                        }
                        break;
                    default: break;
                }
            } while ((opcionMenu != "0") || opcionMenu.Equals(""));
        }
    }
}
