﻿/*Esta clase contiene el juego en sí mismo. Se crean las colecciones necesarias para que funcione, y se vuelca 
 * la información de los archivos en las mismas.
 Constiene el menú principal de la aplicación, los métodos necesarios para el intercambio de información respecto
 de los símbolos del tablero entre archivo y colección.
 También contiene la alternancia de los turnos, entre jugador y máquina y el método que genera el marcador.*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuegoMemory
{
    class Program
    {
        //SE CREA LA COLECCION QUE CONTIENE LOS SÍMBOLOS DE LAS CASILLAS
        public static ArrayList simbolosOrdenados = new ArrayList();
        //SE CREA LA COLECCION QUE ALMACENA EL RANKING DEL JUEGO
        public static List<Ranking> ranking = new List<Ranking>();

        public static void Main(string[] args)
        {
            //VARIABLES PROPIAS DE LA CLASE
            string opcionMenu, nick, contrasenya, mail, nombre, datosOK;
            bool datosCorrectos = false;
            //SE CARGAN LOS USUARIOS A SU COLECCIÓN
            Usuario.LeerFichero();
           // Usuario.PasarJugador();
           //SE VUELCA EL ARCHIVO DE SÍMBOLOS A LA COLECCIÓN
            simbolosOrdenados = volcadoSimbolos();
            //SE VUELVA EL ARCHIVO DE RANKINGS A LA COLECCIÓN
            Ranking.LeerRanking();
            
            //MENÚ BIENVENIDA		
            do
            {
                Console.WriteLine();
                Console.Write("      ");
                Console.WriteLine("============================");
                Console.Write("      ");
                Console.WriteLine("===========MEMORY===========");
                Console.Write("      ");
                Console.WriteLine("============================");
                Console.Write("      ");
                Console.WriteLine();
                Console.WriteLine("Bienvenido al juego de memoria");
                Console.WriteLine();
                Console.WriteLine("Elija una opción:");
                Console.WriteLine();
                Console.WriteLine("1. Registrarse.");
                Console.WriteLine("2. Identificarse. ");
                Console.WriteLine("0. Salir del programa");

                opcionMenu = Console.ReadLine();
                Console.Clear();

                switch(opcionMenu)
                {
                    case "1"://REGISTRARSE
                    {
                        do
                        {

                            do
                            {
                                do
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("====Registrarse====");
                                    Console.WriteLine();
                                    Console.WriteLine("Introduzca el nick:");
                                    nick = Console.ReadLine();
                                    if (Usuario.usuariosList.ContainsKey(nick))
                                    {
                                        Console.WriteLine("El nombre de usuario introducido ya está siendo utilizado" +
                                            " introduzca otro nombre de usuario");
                                    }
                                } while (Usuario.usuariosList.ContainsKey(nick));
                                Console.WriteLine("Introduzca la contraseña:");
                                contrasenya = Console.ReadLine();
                                if (String.IsNullOrEmpty(nick) || String.IsNullOrEmpty(contrasenya))
                                {
                                    Console.WriteLine("El nombre de usuario o la contraseña no pueden estar vacíos");
                                }
                            } while (String.IsNullOrEmpty(nick) || String.IsNullOrEmpty(contrasenya));
                            do
                            {
                                Console.WriteLine("Introduzca su mail:");
                                mail = Console.ReadLine();
                                if (String.IsNullOrEmpty(mail))
                                {
                                    Console.WriteLine("El mail no puede estar vacío");
                                }
                            } while (String.IsNullOrEmpty(mail));
                            do
                            {
                                Console.WriteLine("Introduzca su nombre:");
                                nombre = Console.ReadLine();
                                if (String.IsNullOrEmpty(nombre))
                                {
                                    Console.WriteLine("El nombre no puede estar vacío");
                                }
                            } while (String.IsNullOrEmpty(nombre));                                
                            Console.WriteLine();
                            Console.WriteLine("Los datos introducidos para registrarse son:");
                            Console.WriteLine("Nick: {0}", nick);
                            Console.WriteLine("Contraseña: {0}", contrasenya);
                            Console.WriteLine("Mail: {0}", mail);
                            Console.WriteLine("Nombre: {0}", nombre);
                            Console.WriteLine();
                            Console.WriteLine("Son los datos correctos? (si/no)");
                            datosOK = Console.ReadLine();
                            if (datosOK.Equals("si"))
                            {
                                datosCorrectos = true;
                                Usuario.usuariosList.Add(nick, new Jugador(nick, contrasenya, mail, nombre));
                                Console.WriteLine();
                                Console.WriteLine("Usuario registrado con éxito");
                                Usuario.GuardarUsuarios();
                            }
                        } while (!datosCorrectos);
                    }
                    break;
                    case "2"://IDENTIFICARSE
                    {
                        do
                        {
                            do
                            {
                                Console.WriteLine();
                                Console.WriteLine("====Identificararse====");
                                Console.WriteLine();
                                Console.WriteLine("Introduzca el nombre de usuario:");
                                nick = Console.ReadLine();
                                Console.WriteLine("Introduzca la contraseña:");
                                contrasenya = Console.ReadLine();
                                if (String.IsNullOrEmpty(nick) || String.IsNullOrEmpty(contrasenya))
                                {
                                    Console.WriteLine("El nombre de usuario y la contraseña no pueden estar vacíos");
                                }
                            } while (String.IsNullOrEmpty(nick) || String.IsNullOrEmpty(contrasenya));
                            if (!Usuario.usuariosList.ContainsKey(nick))
                            {
                               Console.WriteLine("Nombre de usuario no encontrado");
                            }
                            else if ((Usuario.usuariosList.ContainsKey(nick)&&
                                    (!Usuario.usuariosList[nick].Contrasenya.Equals(contrasenya))))                    
                                 {
                                     Console.WriteLine("Contraseña no válida");
                                 }
                            else if ((Usuario.usuariosList.ContainsKey(nick) &&
                                    (Usuario.usuariosList[nick].Contrasenya.Equals(contrasenya))))
                                 {
                                    Console.Clear();
                                    if (Usuario.usuariosList[nick] is Administrador)
                                    {
                                        Administrador.MenuAdministrador();
                                    }
                                    if (Usuario.usuariosList[nick] is Jugador)
                                    {
                                        Jugador.MenuJugador(nick);
                                    }
                                 }
                        } while (nick.Equals(null) || contrasenya.Equals(null)
                                    || !Usuario.usuariosList.ContainsKey(nick)
                                    || !Usuario.usuariosList[nick].Contrasenya.Equals(contrasenya));
                        Console.WriteLine();
                    }
                    break;
                    case "0"://SALIR
                    {
                        Usuario.GuardarUsuarios();
                        Console.WriteLine("Fin del programa");
                    }
                    break;
                    default:
                    break;    
                }
            } while (!opcionMenu.Equals("0")||opcionMenu.Equals("")); 
        } //Fin del main
        //METODO QUE CONTIENE EL FUNCIONAMIENTO DEL JUEGO
        public static void InicioJuego(String nick)
        {
            //VARIABLES PROPIAS DEL MÉTODO
            byte contadorJugador=0, contadorOrdenador=0;
            int nivelDificultad=0, tamanyo, fila1, columna1, fila2, columna2;
            Tablero tablero;
            bool turnoJugador = true, juegoTerminado=false, casilla1Correcta=false, casilla2Correcta=false;
            string casilla1Ok, casilla2Ok;
            Random aleatorio = new Random();
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine("====Nueva Partida====");
            do
            {
                Console.WriteLine("Indica el nivel de dificultad (1-3):");
            }while ((!Int32.TryParse(Console.ReadLine(),out nivelDificultad))||
                (nivelDificultad< 1 || nivelDificultad>3)) ;           
            tamanyo = nivelDificultad * 2;//numero de filas y columnas del tablero
            //SE GENERA EL TABLERO DEL JUEGO EN FUNCIÓN DEL NIVEL DE DIFICULTAD
            tablero = new Tablero(tamanyo, prepararSimbolos(nivelDificultad));
            //BUCLE DEL JUEGO
            do
            {
                //TURNO JUGADOR
                if (turnoJugador == true)
                {
                    Tablero.MostrarTablero(tamanyo, tablero);
                    mostrarMarcador(contadorJugador, contadorOrdenador);
                    Console.WriteLine();
                    Console.WriteLine("Turno del jugador");
                    Console.WriteLine();
                    do
                    {
                        Console.WriteLine("Elije la primera casilla");
                        do
                        {
                            Console.WriteLine("Introduce la fila (1-{0}):", nivelDificultad * 2);
                        } while ((!Int32.TryParse(Console.ReadLine(), out fila1)) ||
                            (fila1 < 1 || fila1 > nivelDificultad*2));
                        fila1--;
                        do
                        {
                            Console.WriteLine("Introduce la columna (1-{0}):", nivelDificultad * 2);
                        } while ((!Int32.TryParse(Console.ReadLine(), out columna1)) ||
                            (columna1 < 1 || columna1 > nivelDificultad * 2));
                        columna1--;
                        Console.WriteLine("Ha elegido la casilla Fila {0}, Columna {1}, es correcto? (si/no)"
                            , fila1+1, columna1+1);
                        casilla1Ok = Console.ReadLine();
                        if (casilla1Ok.Equals("si") ) 
                            casilla1Correcta = true;
                        if (tablero.Casillas[fila1, columna1].Simbolo.Equals(" "))
                        {
                            Console.WriteLine("Esa casilla ya no está disponible");
                        }
                    } while (!casilla1Correcta || tablero.Casillas[fila1, columna1].Simbolo.Equals(" "));
                    tablero.Casillas[fila1, columna1].MostrarSímbolo = true;
                    Tablero.MostrarTablero(tamanyo, tablero);
                    mostrarMarcador(contadorJugador, contadorOrdenador);
                    do
                    {
                        Console.WriteLine("Elije la segunda casilla");
                        do
                        {
                            Console.WriteLine("Introduce la fila (1-{0}):", nivelDificultad * 2);
                        } while ((!Int32.TryParse(Console.ReadLine(), out fila2)) ||
                            (fila2 < 1 || fila2 > nivelDificultad * 2));
                        fila2--;
                        do
                        {
                            Console.WriteLine("Introduce la columna (1-{0}):", nivelDificultad * 2);
                        } while ((!Int32.TryParse(Console.ReadLine(), out columna2)) ||
                            (columna2 < 1 || columna2 > nivelDificultad * 2));
                        columna2--;
                        Console.WriteLine("Ha elegido la casilla Fila {0}, Columna {1}, es correcto? (si/no)"
                            , fila2+1, columna2+1);
                        casilla2Ok = Console.ReadLine();
                        if (casilla2Ok.Equals("si"))
                            casilla2Correcta = true;
                        if(tablero.Casillas[fila2,columna2].Simbolo.Equals(" "))
                        {
                            Console.WriteLine("Esa casilla ya no está disponible");
                        }
                        if(fila1==fila2&&columna1==columna2)
                        {
                            Console.WriteLine("No se puede seleccionar dos veces la misma casilla");
                        }
                    } while (!casilla2Correcta || (fila1 == fila2 && columna1 == columna2)
                    || tablero.Casillas[fila2, columna2].Simbolo.Equals(" "));
                    tablero.Casillas[fila2, columna2].MostrarSímbolo = true;
                    Tablero.MostrarTablero(tamanyo, tablero);
                    mostrarMarcador(contadorJugador, contadorOrdenador);
                    if (tablero.Casillas[fila1, columna1].Simbolo.Equals(tablero.Casillas[fila2, columna2].Simbolo))
                    {
                        Console.WriteLine("Correcto!!");
                        Console.WriteLine();
                        tablero.Casillas[fila1, columna1].Simbolo = " ";
                        tablero.Casillas[fila2, columna2].Simbolo = " ";
                        contadorJugador+=2;
                        if ((contadorJugador + contadorOrdenador) >= (nivelDificultad * nivelDificultad * 4))
                        {
                            juegoTerminado = true;
                            Console.WriteLine("Ya no hay más casillas, juego terminado");
                        }
                    }
                    else
                    {
                        Console.WriteLine("No lo has conseguido");
                        turnoJugador = false;
                        tablero.Casillas[fila1, columna1].MostrarSímbolo = false;
                        tablero.Casillas[fila2, columna2].MostrarSímbolo = false;
                    }
                    Console.WriteLine("Pulsa una tecla para continuar... ");
                    Console.ReadKey(true);
                }
                else//TURNO MAQUINA
                {
                    Tablero.MostrarTablero(tamanyo, tablero);
                    mostrarMarcador(contadorJugador, contadorOrdenador);
                    Console.WriteLine();
                    do
                    {
                        do
                        {
                            fila1 = aleatorio.Next(tamanyo);
                            columna1 = aleatorio.Next(tamanyo);
                        } while (tablero.Casillas[fila1,columna1].Simbolo.Equals(" "));
                        tablero.Casillas[fila1, columna1].MostrarSímbolo = true;                  
                        Tablero.MostrarTablero(tamanyo, tablero);
                        mostrarMarcador(contadorJugador, contadorOrdenador);
                        Console.WriteLine("Turno de la máquina");
                        Console.WriteLine("La máquina ha elegido la primera casilla Fila {0}," +
                           " Columna {1}", fila1 + 1, columna1 + 1);
                        Console.WriteLine("Pulsa una tecla para continuar... ");
                        Console.ReadKey(true);
                        do
                        {
                            fila2 = aleatorio.Next(tamanyo);
                            columna2 = aleatorio.Next(tamanyo);
                        } while (tablero.Casillas[fila2, columna2].Simbolo.Equals(" ")
                            ||(fila1==fila2 || columna1==columna2));
                        tablero.Casillas[fila2, columna2].MostrarSímbolo = true;     
                        Tablero.MostrarTablero(tamanyo, tablero);
                        mostrarMarcador(contadorJugador, contadorOrdenador);
                        Console.WriteLine("La máquina ha elegido la segunda casilla Fila {0}," +
                            " Columna {1}", fila2 + 1, columna2 + 1);
                        Console.WriteLine("Pulsa una tecla para continuar... ");
                        Console.ReadKey(true);
                        Console.WriteLine();
                        if (tablero.Casillas[fila1, columna1].Simbolo.Equals(tablero.Casillas[fila2, columna2].Simbolo))
                        {
                            Console.WriteLine("La máquina ha acertado");
                            tablero.Casillas[fila1, columna1].Simbolo = " ";
                            tablero.Casillas[fila2, columna2].Simbolo = " ";
                            contadorOrdenador += 2;
                            if ((contadorJugador + contadorOrdenador) == (nivelDificultad * nivelDificultad * 4))
                            {
                                juegoTerminado = true;
                                Console.WriteLine("Ya no hay más casillas, juego terminado");
                            }
                        }
                        else
                        {
                            Console.WriteLine("La máquina no ha acertado");
                            turnoJugador = true;
                            tablero.Casillas[fila1, columna1].MostrarSímbolo = false;
                            tablero.Casillas[fila2, columna2].MostrarSímbolo = false;
                        }
                        Console.WriteLine("Pulsa una tecla para continuar... ");
                        Console.ReadKey(true);
                    } while (turnoJugador==false && juegoTerminado==false);
                }
            } while (juegoTerminado==false);
            //TERMINA EL JUEGO
            if (contadorJugador > contadorOrdenador)
            {
                Console.WriteLine("Enhorabuena {0}, has ganado!", nick);
                //SE AÑADE AL RANKING SI GANA EL JUGADOR
                ranking.Add(new Ranking(nick, contadorJugador));
            }
            if (contadorOrdenador > contadorJugador)
            {
                Console.WriteLine("Ha ganado la máquina");
            }
            if(contadorJugador==contadorOrdenador)
            {
                Console.WriteLine("Ha habido un empate");
                //SE AÑADE AL RANKING SI HAY UN EMPATE
                ranking.Add(new Ranking(nick, contadorJugador));
            }
            Console.WriteLine();
            Console.WriteLine("Pulsa una tecla para continuar... ");
            Console.ReadKey(true);
            Console.Clear();
        }
        //MÉTODO QUE LEE EL ARCHIVO DONDE SE ALMACENAN LOS SÍMBOLOS Y LOS VUELCA A UN ARRAYLIST
        private static ArrayList volcadoSimbolos()
        {
            ArrayList simbolosOrdenados = new ArrayList();
            string archivo = "simbolos.txt";
            string linea;
            StreamReader fichero = File.OpenText(archivo);
            do
            {
                linea = fichero.ReadLine();
                if (linea != null)
                {
                    simbolosOrdenados.Add(linea);
                }
            } while (linea != null);
            fichero.Close();

            return simbolosOrdenados;
        }
        //MÉTODO QUE GENERA UN ARRAYLIST CON LOS SIMBOLOS PREPARADOS PARA ASIGNARLOS A LAS CASILLAS DEL TABLERO
        private static ArrayList prepararSimbolos(int nivelDificultad)
        {
            ArrayList simbolosTablero = new ArrayList();
            ArrayList simbolosDuplicados = new ArrayList();
            int aleatorio;
            Random r = new Random();
            int simbolosNecesarios=nivelDificultad*nivelDificultad*2;//mitad de las casillas del tablero
            for (int i=0;i<simbolosNecesarios; i++)
            {
                //se añade dos veces porque se necesitan dos símbolos de cada
                simbolosDuplicados.Add(simbolosOrdenados[i]);
                simbolosDuplicados.Add(simbolosOrdenados[i]); 
            }
            while (simbolosDuplicados.Count > 0)
            {
                //se crea un ArrayList con los símbolos necesarios ya duplicados y desordenados
                aleatorio = r.Next(0, simbolosDuplicados.Count-1);
                simbolosTablero.Add(simbolosDuplicados[aleatorio]);
                simbolosDuplicados.RemoveAt(aleatorio);
            }
            return simbolosTablero;
        }
        //MÉTODO PARA VOLCAR EL ARRAYLIST DE SÍMBOLOS AL ARCHIVO
        public static void GrabarSimbolos()
        {
            StreamWriter fichero;
            fichero = File.CreateText("simbolos.txt");
            for (int i = 0; i < simbolosOrdenados.Count; i++)
            {
                fichero.WriteLine(simbolosOrdenados[i]);
            }  
            fichero.Close();
        }
        //MÉTODO PARA MOSTRAR EL MARCADOR DE PUNTOS
        private static void mostrarMarcador(int contadorJugador, int contadormaquina)
        {
            const int PosicionX = 50;
            const int PosicionY = 2;

            Console.SetCursorPosition(PosicionX, PosicionY);
            Console.BackgroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("                  ");
            Console.SetCursorPosition(PosicionX, PosicionY+1);
            Console.Write(" ");
            Console.BackgroundColor = ConsoleColor.Green;
            Console.Write("    MARCADOR    ");
            Console.BackgroundColor = ConsoleColor.DarkMagenta;
            Console.Write(" ");
            Console.SetCursorPosition(PosicionX, PosicionY+2);
            Console.WriteLine("                  ");
            Console.SetCursorPosition(PosicionX, PosicionY+3);
            Console.Write(" ");
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.Write("     JUGADOR    ");
            Console.BackgroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine(" ");
            Console.SetCursorPosition(PosicionX, PosicionY+4);
            Console.Write(" ");
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.Write("    {0} PUNTOS    ", contadorJugador);
            Console.BackgroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine(" ");
            Console.SetCursorPosition(PosicionX, PosicionY+5);
            Console.WriteLine("                  ");
            Console.SetCursorPosition(PosicionX, PosicionY+6);
            Console.Write(" ");
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.Write("     MAQUINA    ");
            Console.BackgroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine(" ");
            Console.SetCursorPosition(PosicionX, PosicionY+7);
            Console.Write(" ");
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.Write("    {0} PUNTOS    ", contadormaquina);
            Console.BackgroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine(" ");
            Console.SetCursorPosition(PosicionX, PosicionY+8);
            Console.WriteLine("                  ");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();

        }
    }
}
