﻿/*En esta clase se establecen los atributos propios de los jugadores, con su método constructor y métodos de 
 * acceso a los atributos. Se generan el struct y el array contiene a los jugadores así como los métodos para 
 * realizar la transferencia de información entre colección y este array.
 *  También contiene el método que desarrolla el menú de los usuarios tipo Jugador*/
 
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuegoMemory
{
    class Jugador : Usuario
    {
        //ATRIBUTOS
        private string mail;
        private string nombre;
        //MÉTODO CONSTRUCTOR
        public Jugador(string nick, string contrasenya, string mail, string nombre):base (nick, contrasenya)
        {
            this.Mail = mail;
            this.Nombre = nombre;
        }
        //MÉTODOS DE ACCESO Y MODIFICACIÓN DE LOS ATRIBUTOS
        public string Mail { get => mail; set => mail = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        //SE SOBREESCRIBE EL MÉTODO ToString() PARA AÑADIR LA INFORMACIÓN A EL ARCHIVO
        public override string ToString()
        {
            return base.ToString()+ Mail + ";" + Nombre + ";";
        }
        //MÉTODO QUE MUESTRA LA INFORMACION DE LOS USUARIOS POR PANTALLA
        public override string Mostrar()
        {
            return base.Mostrar() + ", Mail: " + Mail + ", Nombre: " + Nombre;
        }
        //CREACIÓN DEL STRUCT PARA ALMACENAR LOS JUGADORES
        public struct tipoJugador
        {
            public string nick;
            public string contrasenya;
            public string mail;
            public string nombre;
        }
        //VARIABLES PARA EL MANEJO DEL ARRAY DE STRUCT
        public const int MAX_USUARIOS = 100;
        static int cantidad = 0;
        //CREACIÓN DEL ARRAY PARA ALMACENAR LOS JUGADORES
        public static tipoJugador[] jugadores = new tipoJugador[MAX_USUARIOS];
        //MÉTODO QUE AÑADE LOS JUGADORES AL STRUCT 
        public static void RellenarJugadores(Jugador jugador)
        {
            if (cantidad == MAX_USUARIOS)
            {
                Console.WriteLine("No se pueden añadir más usuarios");
            }
            else
            {
                jugadores[cantidad].nick = jugador.Nick;
                jugadores[cantidad].contrasenya = jugador.Contrasenya;
                jugadores[cantidad].mail = jugador.Mail;
                jugadores[cantidad].nombre = jugador.Nombre;
                cantidad++;
            }
        }
        //MÉTODO QUE CONTIENE EL MENÚ DEL USUARIO TIPO JUGADOR
        public static void MenuJugador(String nick)
        {
            string opcionMenu;
            do
            {
                Console.WriteLine();
                Console.WriteLine("========MENÚ JUGADOR========");
                Console.WriteLine();
                Console.WriteLine("Elija la opción que desea:");
                Console.WriteLine("1. Nueva partida");
                Console.WriteLine("2. Consultar ranking");
                Console.WriteLine("0. Salir del programa");
                opcionMenu = Console.ReadLine();
                Console.WriteLine();

                switch (opcionMenu)
                {
                    case "1"://NUEVA PARTIDA
                    {
                       Program.InicioJuego(nick);
                    }break;

                    case "2"://CONSULTAR RANKING
                    {
                            Ranking.ConsultarRanking();
                    }break;

                    case "0"://SALIR
                    {
                            Ranking.GrabarRanking();
                    }break;
                    default:
                    break;
                }
            } while ((opcionMenu != "0")|| opcionMenu.Equals(""));
        }
    }
}
