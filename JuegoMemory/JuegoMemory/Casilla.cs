﻿/* En esta clase se establecen los atributos de las casillas, que son la unidad básica del juego, con sus métodos constructores y los métodos de acceso y modificación de los mismos. */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuegoMemory
{
    class Casilla
    {
        //ATRIBUTOS 
        private byte fila;
        private byte columna;
        private bool mostrarSimbolo;
        private string simbolo;
        //METODO CONSTRUCTOR CON PARÁMETROS
        public Casilla(byte fila, byte columna, bool mostrarSimbolo, string simbolo)
        {
            this.fila = fila;
            this.columna = columna;
            this.mostrarSimbolo = false;
            this.simbolo = simbolo;
        }
        //MÉTODO CONSTRUCTOR POR DEFECTO
        public Casilla()
        {
            fila = 0;
            columna = 0;
            mostrarSimbolo = false;
            simbolo = "O";
        }
        //MÉTODOS DE ACCESO Y MODIFICACIÓN DE LOS ATRIBUTOS
        public byte Fila { get => fila; set => fila = value; }
        public byte Columna { get => columna; set => columna = value; }
        public bool MostrarSímbolo { get => mostrarSimbolo; set => mostrarSimbolo = value; }
        public string Simbolo { get => simbolo; set => simbolo = value; }
    }
}
