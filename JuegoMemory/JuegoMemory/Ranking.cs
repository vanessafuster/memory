﻿/*En esta clase se establecen los atributos de los elementos tipo Ranking, contiene sus métodos contructores y 
 * los métodos de acceso y modificación del los atributos.
 También contiene todos los métodos necesarios para volcar la información del archivo a la colección y viceversa 
 y para resetear los registros de este ránking*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuegoMemory
{
    class Ranking
    {
        //ATRIBUTOS
        private string nick;
        private int puntuacion;
        private string nombre;
        private DateTime fecha;
        //MÉTODO CONSTRUCTOR CON TODOS LOS PARÁMETROS
        public Ranking(string nick, int puntuacion, string nombre, DateTime fecha)
        {
            this.nick = nick;
            this.puntuacion = puntuacion;
            this.nombre = nombre;
            this.fecha = fecha;
        }
        //MÉTODO CONSTRUCTOR QUE RECIBE LAS PUNTUACIONES DEL JUEGO
        public Ranking(string nick, int puntuacion)
        {
            //usando el struct
            /* int cantidad;
             this.nick = nick;
             this.puntuacion = puntuacion;
             fecha = DateTime.Today;
             cantidad = Jugador.jugadores.Length;
             for(byte i=0; i<cantidad; i++)
             {
                 if (Jugador.jugadores[i].nick.Equals(nick))
                 {
                     nombre = Jugador.jugadores[i].nombre;
                 }
                 else
                     Console.WriteLine("Nick de jugador no encontrado");
             }*/
            //usando la clase
            this.nick = nick;
            this.puntuacion = puntuacion;
            fecha = DateTime.Today;
           /* if (Usuario.usuariosList[nick] is Jugador)
            {
               // nombre = Jugador.usuariosList[nick].Nombre;
            }*/
        }
        //SE SOBREESCRIBE EL MÉTODO ToString() PARA AÑADIR LA INFORMACIÓN A EL ARCHIVO
        public override string ToString()
        {
            return nick+";"+puntuacion+";"+";"+fecha;
          //  return nick + ";" + puntuacion + ";" +nombre+ ";" + fecha;
        }
        //MÉTODO PARA MOSTRAR EL RANKING POR CONSOLA
        public string Mostrar()
        {
            return nick + "     " + puntuacion + " puntos   " + "FECHA " + fecha.Day+"/"+fecha.Month+"/"+fecha.Year;
            //return "NOMBRE  " + nombre + "  " + puntuacion + " puntos   " + "FECHA " + fecha;
        }
        //MÉTODO QUE VUELCA LA INFORMACIÓN DE LA COLECCIÓN AL ARCHIVO
        public static void GrabarRanking()
        {
            StreamWriter fichero;
            fichero = File.CreateText("ranking.txt");
            for(byte i=0; i<Program.ranking.Count; i++)
            {
                fichero.WriteLine(Program.ranking[i].ToString());
            }
            fichero.Close();
        }
        //MÉTODO QUE VUELCA LA INFORMACIÓN DEL ARCHIVO A LA COLECCIÓN
        public static void LeerRanking()
        {
            string linea, nick, nombre;
            string[] lineaPartida;
            int puntuacion, posicion;
            DateTime fecha;

            StreamReader fichero = File.OpenText("ranking.txt");
            do
            {
                linea = fichero.ReadLine();
                if (linea != null)
                {
                    lineaPartida = linea.Split(';');
                    posicion = Convert.ToInt32(lineaPartida[0]);
                    nick = lineaPartida[1];
                    puntuacion = Convert.ToInt32(lineaPartida[2]);
                    nombre = lineaPartida[3];
                    fecha = Convert.ToDateTime(lineaPartida[4]);
                    Program.ranking.Add(new Ranking(nick, puntuacion, nombre, fecha));
                }
            } while (linea != null);
            fichero.Close();
        }
        //MÉTODO QUE MUESTRA LOS DIEZ (SI LOS HAY) MEJORES RESULTADOS POR CONSOLA
        public static void ConsultarRanking()
        {
            int totalRanking;
            //se ordenan los elementos de la lista por puntuación(más alta primero) y por fecha.
            Program.ranking = Program.ranking.OrderByDescending(x => x.puntuacion).OrderBy(x => x.fecha).ToList();
            //se muestran las 10 puntuaciones más altas
            if (Program.ranking.Count > 10)
            {
                totalRanking = 10;
            }   
            else
            {
                totalRanking = Program.ranking.Count;
            }
            for(int i=0; i<totalRanking; i++)
            {
                Console.WriteLine(Program.ranking[i].Mostrar());
            }
        }
        //MÉTODO QUE ELIMINA TODOS LOS REGISTROS DEL RÁNKING
        public static void ResetearRanking()
        {
            StreamWriter fichero;
            fichero = File.CreateText("ranking.txt");           
            fichero.WriteLine();
            fichero.Close();
        }
    }
}
