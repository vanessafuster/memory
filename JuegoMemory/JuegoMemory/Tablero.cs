﻿/*En esta clase se genera el tablero en el que se basa el juego mediante un array de casillas que lo forma.
 Contiene el método constructor, el método de acceso y modificación a las casillas y el método que muestra el tablero
 por la consola*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JuegoMemory
{
    class Tablero
    {
        //SE DECLARA EL ARRAY DE CASILLAS QUE FORMARÁ EL TABLERO, COMO ATRIBUTO DE ESTE
        private Casilla[,] casillas;
        //MÉTODO CONSTRUCTOR QUE INICIALIZA EL ARRAY Y ASIGNA LOS SÍMBOLOS A LAS CASILLAS
        public Tablero(int tamanyo, ArrayList simbolosAleatorios)
        {
            casillas = new Casilla[tamanyo, tamanyo];
            int contador = 0;
            for (byte i=0; i<tamanyo; i++ )
            {
                for(byte j=0; j<tamanyo; j++)
                {
                    casillas[i, j] = new Casilla(i, j, true, simbolosAleatorios[contador].ToString());
                    contador++;
                }
            }
        }
        //MÉTODOS DE ACCESO Y MODIFICACIÓN DE LAS CASILLAS
        public Casilla[,] Casillas { get => casillas; set => casillas = value; }
        //MÉTODO QUE MUESTRA EL TABLERO POR CONSOLA
        public static void MostrarTablero(int tamanyo, Tablero tablero)
        {
            Console.Clear();
            for (int i = 0; i < tamanyo; i++)
            {
                Console.BackgroundColor = ConsoleColor.DarkMagenta;
                Console.SetCursorPosition((4*i+8), 2);
                Console.Write(" C{0}  ", i+1);
            }
            Console.WriteLine();
            for (int i = 0; i < tamanyo; i++)
            {
                Console.BackgroundColor = ConsoleColor.DarkMagenta;
                Console.SetCursorPosition(2,2*i+4);
                Console.Write("Fila {0}", i+1);
                for (int j = 0; j < tamanyo; j++)
                {
                    Console.BackgroundColor = ConsoleColor.DarkBlue;
                    Console.SetCursorPosition((4*j +8), 2*i+4);
                    if (((tablero.casillas[i, j].Simbolo.Equals("/\\"))
                        || (tablero.casillas[i, j].Simbolo.Equals("\\/"))
                        || (tablero.casillas[i, j].Simbolo.Equals( ")("))
                        || (tablero.casillas[i, j].Simbolo.Equals(">>")) 
                        || (tablero.casillas[i, j].Simbolo.Equals("<<" ))
                        || (tablero.casillas[i, j].Simbolo.Equals("<>" ))
                        || (tablero.casillas[i, j].Simbolo.Equals("[]"))
                        || (tablero.casillas[i, j].Simbolo.Equals("]["))
                        || (tablero.casillas[i, j].Simbolo.Equals("><")))&& 
                        tablero.casillas[i, j].MostrarSímbolo == true)
                        Console.Write(" {0}  ", tablero.casillas[i, j].Simbolo);
                    else if(tablero.casillas[i,j].Simbolo.Equals(" "))
                    {
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.Write("  {0}  ", tablero.casillas[i,j].Simbolo);
                    }
                    else if(tablero.casillas[i,j].MostrarSímbolo==false)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkGreen;
                        Console.Write("  O  ");
                    }
                    else
                        Console.Write("  {0}  ", tablero.casillas[i, j].Simbolo);
                }
                Console.WriteLine();
            }
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine();
        }
    }
}
