﻿/*En esta clase se establecen los atributos comunes a todos los usuarios, con su método constructor y métodos de 
 * acceso a los atributos. Se generan tanto la colección como el archivo que almacena estos usuarios y contiene 
 * los métodos para realizar la transferencia de información entre colección y archivo.
 *  También contiene el método que pasa la información de los jugadores a otro método para añadirlo a un array
 *  de Struct*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace JuegoMemory
{
    abstract class Usuario
    {
        //ATRIBUTOS 
        protected string nick;
        protected string contrasenya;
        //MÉTODO CONSTRUCTOR
        protected Usuario(string nick, string contrasenya)
        {
            this.nick = nick;
            this.contrasenya = contrasenya;
        }
        //MÉTODOS DE ACCESO Y MODIFICACIÓN DE LOS ATRIBUTOS
        public string Nick { get => nick; set => nick = value; }
        public string Contrasenya { get => contrasenya; set => contrasenya = value; }

        //CREACIÓN DE LA COLECCIÓN GENÉRICA QUE ALMACENA LOS USUARIOS
        public static SortedList<string,Usuario> usuariosList = new SortedList<string, Usuario>();
        private const string ARCHIVO = "usuarios.txt";
        //SE SOBREESCRIBE EL MÉTODO ToString() PARA AÑADIR LA INFORMACIÓN A EL ARCHIVO
        public override string ToString()
        {
            return Nick + ";" + contrasenya + ";";
        }
        //MÉTODO QUE MUESTRA LA INFORMACION DE LOS USUARIOS POR PANTALLA
        public virtual string Mostrar()
        {
            return "Nick: " + Nick + ", Contraseña: " + Contrasenya;
        }
        //MÉTODO QUE LEE EL ARCHIVO CON LOS USUARIOS REGISTRADOS Y LOS VUELCA EN LA COLECCIÓN      
        public static void LeerFichero()
        {
            string linea;
            StreamReader fichero = File.OpenText(ARCHIVO);
            do
            {
                linea = fichero.ReadLine();
                if(linea!=null)
                {
                    string [] lineaSeparada = linea.Split(';');
                    if(lineaSeparada[0].Equals("admin"))
                    {
                        usuariosList.Add(lineaSeparada[0], new Administrador(lineaSeparada[0], lineaSeparada[1]));
                    }
                    else
                    {
                        usuariosList.Add(lineaSeparada[0],new Jugador(lineaSeparada[0], lineaSeparada[1],
                                               lineaSeparada[2], lineaSeparada[3]));
                    }
                }
            }
            while (linea != null);
            fichero.Close();
        }
        //MÉTODO QUE VUELVA LA COLECCIÓN DE USUARIOS EN EL ARCHIVO
        public static void GuardarUsuarios()
        {
            StreamWriter fich = File.CreateText(ARCHIVO);
            foreach (KeyValuePair<string, Usuario> kvp in usuariosList)
            {
                fich.WriteLine("{0}",kvp.Value.ToString());
            }
            fich.Close();
        }
        //MÉTODO QUE TRANSMITE LA INFORMACIÓN DE LOS JUGADORES PARA RELLENAR EL ARRAY DE STRUCT
        public static void PasarJugador()
        {
            foreach(Jugador j in usuariosList.Values)
            {
                 Jugador.RellenarJugadores(j);
            }
        }
    }
}
